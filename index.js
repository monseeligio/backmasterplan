const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const conexion = require('./conexion');
const path = require('path');
const usuarioRoute = require('./routes/usuario');
const reunionRoute = require('./routes/reuniones');
const docsRoute = require('./routes/docs');
const proyectoRoute = require('./routes/proyectos');
const vtoRoute = require('./routes/vto')

const app = express();

// Configuración de CORS
const corsOptions = {
    origin: ['http://localhost:4200', 'http://149.56.248.111:9095'],
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
    optionsSuccessStatus: 204,
};
app.use(cors(corsOptions));
app.use('/archivos', express.static(path.join(__dirname, 'archivos')));
app.use('/vto', express.static(path.join(__dirname, 'vto')));
app.use('/imgs', express.static(path.join(__dirname, 'imgs')));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use(express.urlencoded({ extended: true }));

app.use(express.json());

app.use('/usuario', usuarioRoute);
app.use('/reunion', reunionRoute);
app.use('/archivo', docsRoute);
app.use('/proyecto', proyectoRoute)
app.use('/vto', vtoRoute)

module.exports = app;
