const express = require('express');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const conexion = require('../conexion');
const fs = require('fs');
require('dotenv').config();
var auth = require('../services/autenticacion');
var checkRole = require('../services/checkrole');

const storage = multer.diskStorage({
    destination: path.join(__dirname, '../vto/'),
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    const allowedFileTypes = ['.png', '.jpg', '.jpeg'];

    if (allowedFileTypes.includes(path.extname(file.originalname).toLowerCase())) {
        cb(null, true);
    } else {
        cb(new Error('Tipo de archivo no permitido'));
    }
}

const upload = multer({
    storage,
    fileFilter,
    limits: {
        fileSize: 2 * 1024 * 1024
    }
});

router.patch('/cambiarVTO/', auth.autentificarToken, checkRole.checkRole, upload.single('imagen'), (req, res, next) => {
    if (!req.file) {
        const error = new Error('No File');
        error.httpStatusCode = 400;
        return next(error);
    }
    const fechaCreacion = new Date();
    const imagenVTO = req.file.filename;
    var query = "INSERT INTO vto (fecha_subida, imagenVTO) VALUES (?, ?)";
    conexion.query(query, [fechaCreacion, imagenVTO], (err, results) => {
        if (!err) {
            return res.status(200).json({ mensaje: "Imagen VTO insertada correctamente" });
        } else {
            console.log(err);
            return res.status(500).json({ error: 'Error al insertar en la base de datos' });
        }
    });
});



router.get('/obtenerUltimaImagen', (req, res) => {
    var query = "SELECT imagenVTO FROM vto ORDER BY fecha_subida DESC LIMIT 1";

    conexion.query(query, (err, results) => {
        if (!err) {
            if (results.length > 0) {
                const ultimaImagen = results[0].imagenVTO;
                return res.json({ url: `http://149.56.248.111:5057/vto/` + ultimaImagen });
            } else {
                return res.status(404).json({ error: 'No se encontraron imágenes' });
            }
        } else {
            console.log(err);
            return res.status(500).json({ error: 'Error al obtener la última imagen' });
        }
    });
});


module.exports = router;
