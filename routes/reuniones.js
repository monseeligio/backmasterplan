const express = require('express');
const conexion = require('../conexion');
const router = express.Router();
const nodemailer = require('nodemailer')
require('dotenv').config();
var auth = require('../services/autenticacion');
var checkRole = require('../services/checkrole');
const { log } = require('async');
const { map } = Array.prototype;
const moment = require('moment');


router.use(express.json())

// REUNIONES

function obtenerUltimoIdReunion() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM reuniones";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`R${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioReunion() {
    try {
        const id = await obtenerUltimoIdReunion();
        return id;
    } catch (error) {
        throw error;
    }
}

router.post('/nuevaReunion', async (req, res) => {
    const reunion = req.body;
    try {
        const idReunion = await idAleatorioReunion();
        let query = "INSERT INTO reuniones(idReunion, nombreReunion, statusReunion) VALUES(?,?,?)";

        conexion.query(query, [idReunion, reunion.nombreReunion, reunion.statusReunion], (err, results) => {
            if (!err) {
                return res.status(200).json({ idReunion, mensaje: 'Registrado correctamente' });
            } else {
                return res.status(500).json({ mensaje: err });
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error });
    }
});

router.patch('/actualizarNombreReunion', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let reunion = req.body;

    var query = "UPDATE reuniones set nombreReunion=? WHERE idReunion=?";
    conexion.query(query, [reunion.nombreReunion, reunion.idReunion], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarStatusReunion', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let reunion = req.body;

    var query = "UPDATE reuniones set statusReunion=? WHERE idReunion=?";
    conexion.query(query, [reunion.statusReunion, reunion.idReunion], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.post('/integrantes', (req, res) => {
    const integrantes = req.body;
    let query = "INSERT INTO participantes(idReunion, idUsuario) VALUES(?,?)";

    conexion.query(query, [integrantes.idReunion, integrantes.idUsuario], (err, results) => {
        if (!err) {
            return res.status(200).json({ mensaje: 'Registrado correctamente' });
        } else {
            return res.status(500).json({ mensaje: err });
        }
    });
});

router.delete('/eliminarIntegrantes/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    const id = req.params.idReunion;
    var query = "DELETE FROM participantes WHERE idReunion=?";
    conexion.query(query, [id], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(results)
            }
            return res.status(200).json({ mensaje: 'Eliminado correctamente' })
        } else {
            return res.status(500).json(err);
        }
    })
})

router.post('/paginas', (req, res) => {
    const paginas = req.body;
    let query = "INSERT INTO paginas(ordenPagina, tipoPagina, urlExternaPagina, tituloPagina, duracionPagina, idReunion) VALUES(?,?,?,?,?,?)";

    conexion.query(query, [paginas.ordenPagina, paginas.tipoPagina, paginas.urlExternaPagina, paginas.tituloPagina, paginas.duracionPagina, paginas.idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json({ mensaje: 'Registrado correctamente' });
        } else {
            return res.status(500).json({ mensaje: err });
        }
    });
});

router.delete('/eliminarPaginas/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    const id = req.params.idReunion;
    var query = "DELETE FROM paginas WHERE idReunion=?";
    conexion.query(query, [id], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(results)
            }
            return res.status(200).json({ mensaje: 'Eliminado correctamente' })
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerReuniones/:idUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idUsuario = req.params.idUsuario;
    var query = "SELECT * FROM participantes WHERE idUsuario=?";
    conexion.query(query, [idUsuario], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerReunionesReunion/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion; // Corregí el nombre de la variable
    var query = "SELECT * FROM reuniones WHERE idReunion=?"; // Añadí la coma al final de la línea
    conexion.query(query, [idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    });
});


router.get('/obtenerDatosReuniones/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion;
    var query = "SELECT reuniones.idReunion, reuniones.nombreReunion, reuniones.statusReunion, usuarios.emailUsuario, usuarios.nombreUsuario, usuarios.apellidoUsuario, usuarios.imagenUsuario, usuarios.idUsuario FROM reuniones JOIN participantes ON reuniones.idReunion = participantes.idReunion JOIN usuarios ON participantes.idUsuario = usuarios.idUsuario WHERE LOWER(reuniones.idReunion) = LOWER(?);";
    conexion.query(query, [idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})


router.get('/obtenerPaginas/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion;
    var query = "SELECT * FROM paginas WHERE idReunion=?";
    conexion.query(query, [idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

//REUNION INICIADA
function obtenerUltimoIdReunionIniciada() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM seguimientoreuniones";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`SR${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioReunionIniciada() {
    try {
        const id = await obtenerUltimoIdReunionIniciada();
        return id;
    } catch (error) {
        throw error;
    }
}


router.post('/nuevaReunionIniciada', async (req, res) => {
    let reunionIniciada = req.body;
    try {
        const idReunionIniciada = await idAleatorioReunionIniciada();
        // Formatear la fecha en el formato deseado
        const fechaFormateada = moment(reunionIniciada.fechaReunionIniciada).format('YYYY-MM-DD HH:mm:ss');
        let query = "INSERT INTO seguimientoreuniones(idReunionIniciada, fechaReunionIniciada,minutoReunionIniciada, minutosPaginas, idReunion) VALUES(?,?,?,?,?)";
        console.log(idReunionIniciada, fechaFormateada, reunionIniciada.minutoReunionIniciada, reunionIniciada.minutosPaginas, reunionIniciada.idReunion)
        conexion.query(query, [idReunionIniciada, fechaFormateada, reunionIniciada.minutoReunionIniciada, reunionIniciada.minutosPaginas, reunionIniciada.idReunion], (err, results) => {
            if (!err) {
                return res.status(200).json({ idReunionIniciada, mensaje: 'Registrado correctamente' });
            } else {
                console.error("Error en la consulta SQL:", err);
                return res.status(500).json({ mensaje: 'Error interno del servidor' });
            }
        });
    } catch (error) {
        console.error("Error al intentar iniciar la reunión:", error);
        return res.status(500).json({ mensaje: 'Error interno del servidor' });
    }
});



router.get('/obtenerIDReunionIniciada/:idReunionIniciada', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunionIniciada = req.params.idReunionIniciada;
    var query = "SELECT * FROM seguimientoreuniones WHERE idReunionIniciada=?";
    conexion.query(query, [idReunionIniciada], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerUltimaReunionIniciada/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion;
    const queryFecha = "SELECT MAX(fechaReunionIniciada) as ultimaReunion FROM seguimientoreuniones WHERE idReunion=?";
    conexion.query(queryFecha, [idReunion], (err, results) => {
        if (!err) {
            const ultimaFecha = results[0].ultimaReunion;
            const queryDatos = "SELECT * FROM seguimientoreuniones WHERE idReunion=? AND fechaReunionIniciada=?";
            conexion.query(queryDatos, [idReunion, ultimaFecha], (err, results) => {
                if (!err) {
                    return res.status(200).json(results);
                } else {
                    return res.status(500).json(err);
                }
            });
        } else {
            return res.status(500).json(err);
        }
    });
});



router.get('/obtenerTiempoReunionIniciada/:idReunionIniciada', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunionIniciada = req.params.idReunionIniciada;
    var query = "SELECT minutoReunionIniciada FROM seguimientoreuniones WHERE idReunionIniciada=?";
    conexion.query(query, [idReunionIniciada], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    });
});

router.patch('/actualizarMinutoReunion', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let reunion = req.body;
    var query = "UPDATE seguimientoreuniones set minutoReunionIniciada=? WHERE idReunionIniciada=?";
    conexion.query(query, [reunion.minutoReunionIniciada, reunion.idReunionIniciada], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarMinutosPaginas', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let reunion = req.body;
    var query = "UPDATE seguimientoreuniones set minutosPaginas=? WHERE idReunionIniciada=?";
    conexion.query(query, [reunion.minutosPaginas, reunion.idReunionIniciada], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})





function idAleatorioIssue() {
    const numeroAleatorio = Math.floor(Math.random() * 90000) + 10000;
    const numeroAleatorioConCeros = String(numeroAleatorio).padStart(5, '0');
    return `I${numeroAleatorioConCeros}`;
}



//INDICADORES
function obtenerUltimoIdMeasurables() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM measurables";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`M${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioMeasurables() {
    try {
        const id = await obtenerUltimoIdMeasurables();
        return id;
    } catch (error) {
        throw error;
    }
}

router.post('/nuevoIndicador', async (req, res) => {
    let measurable = req.body;
    try {
        const idMeasurable = await idAleatorioMeasurables(); // Agrega await aquí
        query = "INSERT INTO measurables(idMeasurable, nombreMeasurable, unidadesMeasurable, tipoMedida, goalMetric, statusMeasurable, idUsuario, idReunion) VALUES(?,?,?,?,?,?,?,?)";
        valores = [idMeasurable, measurable.nombreMeasurable, measurable.unidadesMeasurable, measurable.tipoMedida, measurable.goalMetric, measurable.statusMeasurable, measurable.idUsuario, measurable.idReunion];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json({ mensaje: error });
    }
})

router.patch('/editarIndicador', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let indicador = req.body;

    var query = "UPDATE measurables set nombreMeasurable=?, tipoMedida=?, goalMetric=?, idUsuario=? WHERE idMeasurable=?";
    conexion.query(query, [indicador.nombreMeasurable, indicador.tipoMedida, indicador.goalMetric, indicador.idUsuario, indicador.idMeasurable], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            console.log(err);
            return res.status(500).json(err);
        }
    })
})

router.patch('/archivarIndicador', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let indicador = req.body;

    var query = "UPDATE measurables set statusMeasurable=? WHERE idMeasurable=?";
    conexion.query(query, [indicador.statusMeasurable, indicador.idMeasurable], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Archivado correctamente" });
        }
        else {
            console.log(err);
            return res.status(500).json(err);
        }
    })
})

router.post('/nuevoSeguimientoIndicador', async (req, res) => {
    let measurable = req.body;
    try {
        query = "INSERT INTO seguimientomeasurables(fechaMeasurable, cumplimientoMeasurable, idMeasurable) VALUES(?,?,?)";
        valores = [measurable.fechaMeasurable, measurable.cumplimientoMeasurable, measurable.idMeasurable];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json({ mensaje: error });
    }
})

router.patch('/actualizarSeguimientoIndicador', async (req, res) => {
    let measurable = req.body;
    try {
        query = "UPDATE seguimientomeasurables SET cumplimientoMeasurable=? WHERE fechaMeasurable=? AND idMeasurable=?";
        valores = [measurable.cumplimientoMeasurable, measurable.fechaMeasurable, measurable.idMeasurable];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json({ mensaje: error });
    }
})

router.get('/obtenerMeasurables/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion;
    var query = "SELECT measurables.idMeasurable, measurables.nombreMeasurable, measurables.unidadesMeasurable, measurables.tipoMedida, measurables.goalMetric, measurables.statusMeasurable, measurables.idUsuario, usuarios.nombreUsuario, usuarios.apellidoUsuario, usuarios.emailUsuario, usuarios.imagenUsuario FROM measurables JOIN usuarios ON measurables.idUsuario = usuarios.idUsuario WHERE measurables.idReunion=? ORDER BY measurables.idMeasurable ASC";
    conexion.query(query, [idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerCumplimientoMeasurables/:idMeasurable', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idMeasurable = req.params.idMeasurable;
    var query = "SELECT fechaMeasurable, cumplimientoMeasurable, idMeasurable FROM seguimientomeasurables WHERE idMeasurable=?";
    conexion.query(query, [idMeasurable], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerCumplimientoFechasMeasurables/:idMeasurable/:fechaMeasurable', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    let idMeasurable = req.params.idMeasurable;
    let fechaMeasurable = req.params.fechaMeasurable;
    var query = "SELECT cumplimientoMeasurable FROM seguimientomeasurables WHERE fechaMeasurable=? AND idMeasurable=?";
    conexion.query(query, [fechaMeasurable, idMeasurable], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    });
});


//ROCAS
function obtenerUltimoIdRocas() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM goals";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`G${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioRoca() {
    try {
        const id = await obtenerUltimoIdRocas();
        return id;
    } catch (error) {
        throw error;
    }
}


router.post('/nuevaMeta', async (req, res) => {
    let roca = req.body;
    try {
        const idGoal = await idAleatorioRoca(); // Agrega await aquí
        query = "INSERT INTO goals(idGoal, tituloGoal, statusGoal, realGoal, acumuladoGoal, porcentajeGoal, idUsuario, idReunion) VALUES(?,?,?,?,?,?,?,?)";
        valores = [idGoal, roca.tituloGoal, roca.statusGoal, roca.realGoal, roca.acumuladoGoal, roca.porcentajeGoal, roca.idUsuario, roca.idReunion];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json({ mensaje: error.sqlMessage });
    }
})

router.patch('/editarMeta', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let roca = req.body;

    var query = "UPDATE goals set tituloGoal=?, idUsuario=? WHERE idGoal=?";
    conexion.query(query, [roca.tituloGoal, roca.idUsuario, roca.idGoal], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            console.log(err);
            return res.status(500).json(err);
        }
    })
})

router.patch('/archivarMeta', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let roca = req.body;

    var query = "UPDATE goals set statusGoal=? WHERE idGoal=?";
    conexion.query(query, [roca.statusGoal, roca.idGoal], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Archivado correctamente" });
        }
        else {
            console.log(err);
            return res.status(500).json(err);
        }
    })
})



router.get('/obtenerMetas/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion;
    var query = "SELECT goals.idGoal, goals.tituloGoal,goals.realGoal, goals.acumuladoGoal, goals.porcentajeGoal, goals.statusGoal, goals.idUsuario, usuarios.nombreUsuario, usuarios.apellidoUsuario, usuarios.emailUsuario, usuarios.imagenUsuario FROM goals JOIN usuarios ON goals.idUsuario = usuarios.idUsuario WHERE goals.idReunion=?";
    conexion.query(query, [idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerPlaneadoMes/:mesAnioPlan/:idGoal', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const goal = req.params.idGoal;
    const mesAnioPlan = req.params.mesAnioPlan
    var query = "SELECT * FROM planeadogoals WHERE mesAnioPlan=? AND idGoal=?";
    conexion.query(query, [mesAnioPlan, goal], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.post('/guardarPlaneadoMes', async (req, res) => {
    let plan = req.body;
    try {
        query = "INSERT INTO planeadogoals(mesAnioPlan, planeadoGoal, realMesGoal, idGoal) VALUES(?,?,?,?)";
        valores = [plan.mesAnioPlan, plan.planeadoGoal, plan.realMesGoal, plan.idGoal];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json({ mensaje: error });
    }
})

router.patch('/actualizarPlaneadoMes', async (req, res) => {
    let plan = req.body;
    try {
        query = "UPDATE planeadoGoals SET planeadoGoal=? WHERE mesAnioPlan=? AND idGoal=?";
        valores = [plan.planeadoGoal, plan.mesAnioPlan, plan.idGoal];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Actualizado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json({ mensaje: error });
    }
})

router.get('/obtenerSeguimientoRocas/:fechaGoal/:idGoal', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const goal = req.params.idGoal;
    const fecha = req.params.fechaGoal
    var query = "SELECT * FROM seguimientoGoals WHERE fechaGoal=? AND idGoal=?";
    conexion.query(query, [fecha, goal], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.post('/guardarSeguimiento', async (req, res) => {
    let seguimiento = req.body;
    try {
        query = "INSERT INTO seguimientogoals(fechaGoal, cumplimientoGoal, comentariosGoal, idGoal) VALUES(?,?,?,?)";
        valores = [seguimiento.fechaGoal, seguimiento.cumplimientoGoal, seguimiento.comentariosGoal, seguimiento.idGoal];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json({ mensaje: error });
    }
})

router.patch('/actualizarSeguimiento', async (req, res) => {
    let seguimiento = req.body;
    try {
        query = "UPDATE seguimientoGoals SET cumplimientoGoal=? WHERE fechaGoal=? AND idGoal=?";
        valores = [seguimiento.cumplimientoGoal, seguimiento.fechaGoal, seguimiento.idGoal];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Actualizado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json(error);
    }
})


router.patch('/sumarRealMes', async (req, res) => {
    let suma = req.body;
    try {
        const selectQuery = "SELECT realMesGoal FROM planeadoGoals WHERE idGoal=?";
        const selectValues = [suma.idGoal];

        conexion.query(selectQuery, selectValues, (selectErr, selectResults) => {
            if (selectErr) {
                return res.status(500).json({ mensaje: selectErr.sqlMessage });
            }

            if (selectResults.length === 0) {
                return res.status(404).json({ mensaje: 'No se encontró el goal con el ID proporcionado' });
            }

            const realMesGoalActual = selectResults[0].realMesGoal;
            const nuevoRealMesGoal = realMesGoalActual + suma.cumplimientoGoal;

            const updateQuery = "UPDATE planeadoGoals SET realMesGoal=? WHERE idGoal=?";
            const updateValues = [nuevoRealMesGoal, suma.idGoal];

            conexion.query(updateQuery, updateValues, (updateErr, updateResults) => {
                if (!updateErr) {
                    return res.status(200).json({ mensaje: 'Actualizado correctamente' });
                } else {
                    return res.status(500).json({ mensaje: updateErr.sqlMessage });
                }
            });
        });
    } catch (error) {
        return res.status(500).json(error);
    }
});

router.patch('/actualizarRealMes', async (req, res) => {
    let suma = req.body;
    try {
        const selectQuery = "SELECT realMesGoal FROM planeadoGoals WHERE idGoal=?";
        const selectValues = [suma.idGoal];

        conexion.query(selectQuery, selectValues, (selectErr, selectResults) => {
            if (selectErr) {
                return res.status(500).json({ mensaje: selectErr.sqlMessage });
            }

            if (selectResults.length === 0) {
                return res.status(404).json({ mensaje: 'No se encontró el goal con el ID proporcionado' });
            }

            const realMesGoalActual = selectResults[0].realMesGoal;
            const nuevoRealMesGoal = parseFloat((realMesGoalActual - suma.datoAnterior) + suma.cumplimientoGoal);

            const updateQuery = "UPDATE planeadoGoals SET realMesGoal=? WHERE idGoal=?";
            const updateValues = [nuevoRealMesGoal, suma.idGoal];

            conexion.query(updateQuery, updateValues, (updateErr, updateResults) => {
                if (!updateErr) {
                    return res.status(200).json({ mensaje: 'Actualizado correctamente' });
                } else {
                    return res.status(500).json({ mensaje: updateErr.sqlMessage });
                }
            });
        });
    } catch (error) {
        return res.status(500).json(error);
    }
});

router.patch('/sumarPorcentaje', async (req, res) => {
    let suma = req.body;
    try {
        const selectQuery = "SELECT porcentajeGoal FROM Goals WHERE idGoal=?";
        const selectValues = [suma.idGoal];

        conexion.query(selectQuery, selectValues, (selectErr, selectResults) => {
            if (selectErr) {
                return res.status(500).json({ mensaje: selectErr.sqlMessage });
            }

            if (selectResults.length === 0) {
                return res.status(404).json({ mensaje: 'No se encontró el goal con el ID proporcionado' });
            }

            const realMesGoalActual = selectResults[0].porcentajeGoal;
            const nuevoRealMesGoal = realMesGoalActual + suma.sumaPorcentaje;

            const updateQuery = "UPDATE goals SET porcentajeGoal=? WHERE idGoal=?";
            const updateValues = [nuevoRealMesGoal, suma.idGoal];

            conexion.query(updateQuery, updateValues, (updateErr, updateResults) => {
                if (!updateErr) {
                    return res.status(200).json({ mensaje: 'Actualizado correctamente' });
                } else {
                    return res.status(500).json({ mensaje: updateErr.sqlMessage });
                }
            });
        });
    } catch (error) {
        return res.status(500).json(error);
    }
});

router.patch('/actualizarPorcentaje', async (req, res) => {
    let suma = req.body;
    try {
        const selectQuery = "SELECT porcentajeGoal FROM Goals WHERE idGoal=?";
        const selectValues = [suma.idGoal];

        conexion.query(selectQuery, selectValues, (selectErr, selectResults) => {
            if (selectErr) {
                return res.status(500).json({ mensaje: selectErr.sqlMessage });
            }

            if (selectResults.length === 0) {
                return res.status(404).json({ mensaje: 'No se encontró el goal con el ID proporcionado' });
            }

            const realMesGoalActual = selectResults[0].porcentajeGoal;
            const nuevoRealMesGoal = parseFloat((realMesGoalActual - suma.datoAnterior) + suma.sumaPorcentaje);

            const updateQuery = "UPDATE goals SET porcentajeGoal=? WHERE idGoal=?";
            const updateValues = [nuevoRealMesGoal, suma.idGoal];

            conexion.query(updateQuery, updateValues, (updateErr, updateResults) => {
                if (!updateErr) {
                    return res.status(200).json({ mensaje: 'Actualizado correctamente' });
                } else {
                    return res.status(500).json({ mensaje: updateErr.sqlMessage });
                }
            });
        });
    } catch (error) {
        return res.status(500).json(error);
    }
});

router.get('/obtenerComentario/:fechaGoal/:idGoal', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const goal = req.params.idGoal;
    const fecha = req.params.fechaGoal
    var query = "SELECT comentariosGoal FROM seguimientoGoals WHERE fechaGoal=? AND idGoal=?";
    conexion.query(query, [fecha, goal], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/agregarComentario', async (req, res) => {
    let comentario = req.body;
    try {
        query = "UPDATE seguimientoGoals SET comentariosGoal=? WHERE fechaGoal=? AND idGoal=?";
        valores = [comentario.comentariosGoal, comentario.fechaGoal, comentario.idGoal];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Actualizado correctamente' });
            }
            else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        })
    } catch (error) {
        return res.status(500).json(error);
    }
})

// HEADLINES 
function obtenerUltimoIdAsuntos() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM headlines";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`H${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioAsunto() {
    try {
        const id = await obtenerUltimoIdAsuntos();
        return id;
    } catch (error) {
        throw error;
    }
}

router.post('/nuevoAsunto', async (req, res) => {
    let asunto = req.body;
    try {
        let idHeadline = await idAleatorioAsunto();
        query = "INSERT INTO headlines(idHeadline, nombreHeadline, detallesHeadline, statusHeadline, idUsuario, idReunion, idReunionIniciada) VALUES(?,?,?,?,?,?,?)";
        valores = [idHeadline, asunto.nombreHeadline, asunto.detallesHeadline, asunto.statusHeadline, asunto.idUsuario, asunto.idReunion, asunto.idReunionIniciada];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            } else {
                console.log(err);
                if (err.sqlMessage.includes("Duplicate entry")) {
                    return res.status(400).json({ mensaje: "Algo salió mal. Inténtalo de nuevo." });
                } else {
                    return res.status(500).json({ mensaje: err.sqlMessage });
                }
            }
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ mensaje: error.message });
    }
})

router.get('/obtenerAsuntos/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion;
    var query = "SELECT headlines.idHeadline, headlines.nombreHeadline, headlines.detallesHeadline, headlines.statusHeadline, headlines.idUsuario, usuarios.nombreUsuario, usuarios.apellidoUsuario, emailUsuario, usuarios.imagenUsuario FROM headlines JOIN usuarios ON headlines.idUsuario = usuarios.idUsuario WHERE headlines.idReunion=?";
    conexion.query(query, [idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerAsuntosResueltos/:idReunionIniciada', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunionIniciada = req.params.idReunionIniciada;
    var query = "SELECT headlines.idHeadline, headlines.nombreHeadline, headlines.detallesHeadline, headlines.statusHeadline, headlines.idUsuario, usuarios.nombreUsuario, usuarios.apellidoUsuario, emailUsuario, usuarios.imagenUsuario FROM headlines JOIN usuarios ON headlines.idUsuario = usuarios.idUsuario WHERE headlines.idReunionIniciada=? AND headlines.statusHeadline = 'Visto'";
    conexion.query(query, [idReunionIniciada], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    });
});

router.patch('/actualizarTituloAsunto', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let asunto = req.body;

    var query = "UPDATE headlines set nombreHeadline=? WHERE idHeadline=?";
    conexion.query(query, [asunto.nombreHeadline, asunto.idHeadline], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarUsuarioAsunto', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let asunto = req.body;

    var query = "UPDATE headlines set idUsuario=? WHERE idHeadline=?";
    conexion.query(query, [asunto.idUsuario, asunto.idHeadline], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarStatusAsunto', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let asunto = req.body;

    var query = "UPDATE headlines set statusHeadline=? WHERE idReunionIniciada=?";
    conexion.query(query, [asunto.statusHeadline, asunto.idReunionIniciada], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarStatusAsuntoNull', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let asunto = req.body;
    var query = `
        UPDATE headlines 
        SET idReunionIniciada=? 
        WHERE (idReunionIniciada IS NULL AND idReunion=?) 
            OR (idReunionIniciada!=? AND statusHeadline!='Visto')
    `;
    conexion.query(query, [asunto.idReunionIniciada, asunto.idReunion, asunto.idReunionIniciada], (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ mensaje: 'Error interno del servidor' });
        }
        if (results.affectedRows > 0) {
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        } else {
            return res.status(200).json({ mensaje: "No se encontró ningún asunto para actualizar" });
        }
    });
});




// ACTIVIDADES
function obtenerUltimoIdActividades() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM todos";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`T${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioTodo() {
    try {
        const id = await obtenerUltimoIdActividades();
        return id;
    } catch (error) {
        throw error;
    }
}

router.post('/nuevaActividad', async (req, res) => {
    let todo = req.body;
    try {
        let idTodo = await idAleatorioTodo();

        query = "INSERT INTO todos(idTodo, tituloToDo, detallesToDo, dueDateToDo, statusToDo, idUsuario, idReunion, idReunionIniciada) VALUES(?,?,?,?,?,?,?,?)";
        valores = [idTodo, todo.tituloToDo, todo.detallesToDo, todo.dueDateToDo, todo.statusToDo, todo.idUsuario, todo.idReunion, todo.idReunionIniciada];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            } else {
                if (err.sqlMessage.includes("Duplicate entry")) {
                    return res.status(400).json({ mensaje: "Algo salió mal. Inténtalo de nuevo." });
                } else {
                    return res.status(500).json({ mensaje: err.sqlMessage });
                }
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
});

router.get('/obtenerActividades/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion;
    var query = "SELECT todos.idToDo, todos.tituloToDo, todos.detallesToDo, todos.dueDateToDo, todos.statusToDo, todos.idUsuario, todos.idReunionIniciada, usuarios.nombreUsuario, usuarios.apellidoUsuario, usuarios.emailUsuario, usuarios.imagenUsuario FROM todos JOIN usuarios ON todos.idUsuario = usuarios.idUsuario WHERE todos.idReunion=?";
    conexion.query(query, [idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarTituloActividad', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let actividad = req.body;

    var query = "UPDATE todos set tituloToDo=? WHERE idToDo=?";
    conexion.query(query, [actividad.tituloToDo, actividad.idToDo], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarFechaToDo', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let actividad = req.body;

    var query = "UPDATE todos set dueDateToDo=? WHERE idToDo=?";
    conexion.query(query, [actividad.dueDateToDo, actividad.idToDo], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarUsuarioToDo', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let actividad = req.body;

    var query = "UPDATE todos set idUsuario=? WHERE idToDo=?";
    conexion.query(query, [actividad.idUsuario, actividad.idToDo], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarStatusToDo', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let actividad = req.body;

    var query = "UPDATE todos set statusToDo=? WHERE idToDo=?";
    conexion.query(query, [actividad.statusToDo, actividad.idToDo], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            console.log(err);
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarStatusActividadNull', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let actividad = req.body;

    var query = "UPDATE todos set idReunionIniciada=? WHERE idReunionIniciada IS NULL AND idReunion=?";
    conexion.query(query, [actividad.idReunionIniciada, actividad.idReunion], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(200).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})


// PROBLEMAS
function obtenerUltimoIdIssues() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM issues";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`I${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioIssue() {
    try {
        const id = await obtenerUltimoIdIssues();
        return id;
    } catch (error) {
        throw error;
    }
}

router.post('/nuevoProblema', async (req, res) => {
    let problema = req.body;
    try {
        let idIssue = await idAleatorioIssue();
        query = "INSERT INTO issues(idIssue, nombreIssue, detallesIssue, fechaIssue, statusIssue, idUsuario, idReunion, idReunionIniciada) VALUES(?,?,?,?,?,?,?,?)";
        valores = [idIssue, problema.nombreIssue, problema.detallesIssue, problema.fechaIssue, problema.statusIssue, problema.idUsuario, problema.idReunion, problema.idReunionIniciada];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            } else {
                if (err.sqlMessage.includes("Duplicate entry")) {
                    return res.status(400).json({ mensaje: "Algo salió mal. Inténtalo de nuevo." });
                } else {
                    return res.status(500).json({ mensaje: err.sqlMessage });
                }
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
});

router.get('/obtenerProblemas/:idReunion', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunion = req.params.idReunion;
    var query = "SELECT issues.idIssue, issues.nombreIssue, issues.detallesIssue, issues.fechaIssue,  issues.statusIssue, issues.numeroIssue, issues.idUsuario,usuarios.nombreUsuario, usuarios.apellidoUsuario, usuarios.emailUsuario, usuarios.imagenUsuario FROM issues JOIN usuarios ON issues.idUsuario = usuarios.idUsuario WHERE issues.idReunion=? ORDER BY CASE WHEN issues.numeroIssue IS NULL THEN 1 ELSE 0 END, issues.numeroIssue ASC;";
    conexion.query(query, [idReunion], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerProblemasResueltos/:idReunionIniciada', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idReunionIniciada = req.params.idReunionIniciada;
    var query = "SELECT issues.idIssue, issues.nombreIssue, issues.detallesIssue, issues.fechaIssue,  issues.statusIssue, issues.numeroIssue, issues.idUsuario, usuarios.nombreUsuario, usuarios.apellidoUsuario, usuarios.emailUsuario, usuarios.imagenUsuario FROM issues JOIN usuarios ON issues.idUsuario = usuarios.idUsuario WHERE issues.idReunionIniciada=? AND issues.statusIssue = 'Resuelto'";
    conexion.query(query, [idReunionIniciada], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    });
});


router.patch('/actualizarTituloProblema', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let problema = req.body;

    var query = "UPDATE issues set nombreIssue=? WHERE idIssue=?";
    conexion.query(query, [problema.nombreIssue, problema.idIssue], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})


router.patch('/actualizarUsuarioIssue', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let problema = req.body;

    var query = "UPDATE issues set idUsuario=? WHERE idIssue=?";
    conexion.query(query, [problema.idUsuario, problema.idIssue], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarStatusIssue', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let problema = req.body;

    var query = "UPDATE issues set statusIssue=?, numeroIssue=NULL WHERE idIssue=?";
    conexion.query(query, [problema.statusIssue, problema.idIssue], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarStatusProblemaNull', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let problema = req.body;
    var query = `
        UPDATE issues 
        SET idReunionIniciada=? 
        WHERE (idReunionIniciada IS NULL AND idReunion=?) 
            OR (idReunionIniciada!=? AND statusIssue!='Resuelto')
    `;
    conexion.query(query, [problema.idReunionIniciada, problema.idReunion, problema.idReunionIniciada], (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ mensaje: 'Error interno del servidor' });
        }
        if (results.affectedRows > 0) {
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        } else {
            return res.status(200).json({ mensaje: "No se encontró ningún problema para actualizar" });
        }
    });
});

router.patch('/numerarIssue', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let problema = req.body;

    var query = "UPDATE issues set numeroIssue=? WHERE idIssue=?";
    conexion.query(query, [problema.numeroIssue, problema.idIssue], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/quitarNumeroIssue', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let problema = req.body;

    var query = "UPDATE issues set numeroIssue=? WHERE idIssue=?";
    conexion.query(query, [problema.numeroIssue, problema.idIssue], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.post('/enviarCorreo', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    let datos = req.body
    enviarCorreo(datos, info => {
        res.send(info)
    })
});

async function enviarCorreo(variables, callback) {
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: "masterplanparautos@gmail.com",
            pass: "vllx kegv hhhm pshh",
        },
    })

    let htmlActividades = '<h2 style="text-align: center;">Actividades</h2>';
    for (const usuarioKey in variables.actividades) {
        if (variables.actividades.hasOwnProperty(usuarioKey)) {
            const usuario = variables.actividades[usuarioKey];
            htmlActividades += `<h3 style="font-weight: bold;">${usuario[0].nombreUsuario} ${usuario[0].apellidoUsuario}</h3>`;
            for (const actividad of usuario) {
                htmlActividades += '<div style="display: flex; justify-content: space-between; align-items: flex-end; margin-bottom: 8px;">';
                htmlActividades += '<div style="flex: 1;">';
                htmlActividades += `<span>${actividad.tituloToDo}</span>`;
                htmlActividades += '</div>';
                htmlActividades += '<div style="font-weight: bold; text-align: right; flex-shrink: 0;">';
                if (!tarde(actividad.dueDateToDo)) {
                    htmlActividades += `${formatoFecha(actividad.dueDateToDo)}`;
                } else {
                    htmlActividades += `<span style="color: red;">${formatoFecha(actividad.dueDateToDo)}</span>`;
                }
                htmlActividades += '</div>';
                htmlActividades += '</div>';
            }
        }
    }
    htmlActividades += '<hr>';

    let htmlAsuntos = '<h2 style="text-align: center;">Asuntos</h2>';
    for (const usuarioKey in variables.asuntos) {
        if (variables.asuntos.hasOwnProperty(usuarioKey)) {
            const usuario = variables.asuntos[usuarioKey];
            htmlAsuntos += `<h3 style="font-weight: bold;">${usuario[0].nombreUsuario} ${usuario[0].apellidoUsuario}</h3>`;
            for (const asunto of usuario) {
                htmlAsuntos += '<div style="display: flex; justify-content: space-between; align-items: flex-end; margin-bottom: 8px;">';
                htmlAsuntos += '<div style="flex: 1;">';
                htmlAsuntos += `<span>${asunto.nombreHeadline}</span>`;
                htmlAsuntos += '</div>';
                htmlAsuntos += '</div>';
            }
        }
    }
    htmlAsuntos += '<hr>';

    let htmlProblemas = '<h2 style="text-align: center;">IDS</h2>';
    for (const usuarioKey in variables.problemas) {
        if (variables.problemas.hasOwnProperty(usuarioKey)) {
            const usuario = variables.problemas[usuarioKey];
            htmlProblemas += `<h3 style="font-weight: bold;">${usuario[0].nombreUsuario} ${usuario[0].apellidoUsuario}</h3>`;
            for (const problemas of usuario) {
                htmlProblemas += '<div style="display: flex; justify-content: space-between; align-items: flex-end; margin-bottom: 8px;">';
                htmlProblemas += '<div style="flex: 1;">';
                htmlProblemas += `<span>${problemas.nombreIssue}</span>`;
                htmlProblemas += '</div>';
                htmlProblemas += '</div>';
            }
        }
    }

    let mailOptions = {
        from: 'masterplanparautos@gmail.com',
        to: variables.participantes.join(','),
        subject: `Resumen de la reunion ${variables.nombreReunion}`,
        html: `<!DOCTYPE html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Resumen de Reunión</title>
          <style>
          .card {
            width: 40%;
            margin: 10px;
            border: 1px solid #ddd;
            border-radius: 8px;
            padding: 20px;
            justify-content: center;
            text-align: center;
        }

            .card img {
              width: 80px;
            }

            .card-container {
                display: flex;
                flex-wrap: wrap;
                width: 100%;
                justify-content: center;
                text-align: center;
            }
          </style>
        </head>
        <body>
        <img src="cid:logo" width="100" alt="Logo" style="text-align:center;">
          <h1 style="text-align: center">MasterPlan</h1>
          <h3>Hola! Aquí tienes tu resumen de la reunión ${variables.nombreReunion}</h3>
          
          <div class="card-container">
            <div class="card">
              <img src="cid:issuesResueltos" width="85" alt="Problemas resueltos">
              <h3>Problemas resueltos</h3>
              <h4>${variables.issuesResueltos}</h4>
            </div>
            <div class="card">
              <img src="cid:porcentajeActividades" width="85" alt="Porcentaje de actividades">
              <h3>Porcentaje de actividades</h3>
              <h4>${variables.porcentaje}%</h4>
            </div>
          </div>
          <div class="card-container">
            <div class="card">
              <img src="cid:calificacion" width="85" alt="Calificación">
              <h3>Calificación</h3>
              <h4>${variables.promedio}</h4>
            </div>
            <div class="card">
              <img src="cid:tiempo" width="85" alt="Tiempo">
              <h3>Tiempo</h3>
              <h4>${variables.tiempo} minutos</h4>
            </div>
          </div>
          <hr>

          <!-- Actividades -->
          ${htmlActividades}
          <!-- Asuntos -->
          ${htmlAsuntos}
          <!-- Problemas -->
          ${htmlProblemas}
        </body>
        </html>`,
        attachments: [
            {
                filename: 'logo.png',
                path: './imgs/correos/Logo.png',
                cid: 'logo'
            },
            {
                filename: 'issuesResueltos.png',
                path: './imgs/correos/NoProblemas.png',
                cid: 'issuesResueltos'
            },
            {
                filename: 'porcentajeActividades.png',
                path: './imgs/correos/NoActividades.png',
                cid: 'porcentajeActividades'
            },
            {
                filename: 'calificacion.png',
                path: './imgs/correos/Promedio.png',
                cid: 'calificacion'
            },
            {
                filename: 'tiempo.png',
                path: './imgs/correos/Tiempo.png',
                cid: 'tiempo'
            },
        ]
    }

    let info = await transporter.sendMail(mailOptions);

    callback(info)
}

function tarde(fecha) {
    const fechaHoy = new Date();
    const fechaParametro = new Date(fecha);
    return fechaParametro < fechaHoy;
}

function formatoFecha(fecha) {
    const fechaObj = new Date(fecha);
    const dia = fechaObj.getDate();
    const mes = fechaObj.getMonth() + 1;
    const año = fechaObj.getFullYear();
    return `${dia.toString().padStart(2, '0')}/${mes.toString().padStart(2, '0')}/${año}`;
}


module.exports = router;