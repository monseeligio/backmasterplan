const express = require('express');
const router = express.Router();
const Joi = require('@hapi/joi');
require('dotenv').config();
const conexion = require('../conexion');
var auth = require('../services/autenticacion');
var checkRole = require('../services/checkrole');

function obtenerUltimoIdRocas() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM rocas";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`R${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioRoca() {
    try {
        const id = await obtenerUltimoIdRocas();
        return id;
    } catch (error) {
        throw error;
    }
}

router.post('/nuevaRoca', async (req, res) => {
    let roca = req.body;
    try {
        let idRoca = await idAleatorioRoca();
        query = "INSERT INTO rocas(idRoca, nombreRoca, statusRoca, idUsuario) VALUES(?,?,?,?)";
        valores = [idRoca, roca.nombreRoca, roca.statusRoca, roca.idUsuario];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            } else {
                if (err.sqlMessage.includes("Duplicate entry")) {
                    return res.status(400).json({ mensaje: "Algo salió mal. Inténtalo de nuevo." });
                } else {
                    return res.status(500).json({ mensaje: err.sqlMessage });
                }
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
})

router.patch('/editarRoca', async (req, res) => {
    let roca = req.body;
    try {
        query = "UPDATE rocas SET nombreRoca=? WHERE idRoca=?";
        valores = [roca.nombreRoca, roca.idRoca];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Actuaizado correctamente' });
            } else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
})

router.patch('/archivarRoca', async (req, res) => {
    let roca = req.body;
    try {
        query = "UPDATE rocas SET statusRoca='Archivado' WHERE idRoca=?";
        valores = [roca.idRoca];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Actuaizado correctamente' });
            } else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
})

router.get('/obtenerRocas/:idUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idUsuario = req.params.idUsuario;
    var query = "SELECT rocas.idRoca, rocas.nombreRoca, rocas.statusRoca, rocas.idUsuario, usuarios.nombreUsuario, usuarios.apellidoUsuario, usuarios.emailUsuario, usuarios.imagenUsuario FROM rocas JOIN usuarios ON rocas.idUsuario = usuarios.idUsuario WHERE rocas.idUsuario=?";
    conexion.query(query, [idUsuario], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})


function obtenerUltimoIdActividades() {
    return new Promise((resolve, reject) => {
        const query = "SELECT COUNT(*) as total FROM actividadesRoca";
        conexion.query(query, (err, results) => {
            if (!err) {
                const total = results[0].total;
                const nuevoId = total + 1;
                resolve(`A${nuevoId}`);
            } else {
                reject(err);
            }
        });
    });
}

async function idAleatorioActividad() {
    try {
        const id = await obtenerUltimoIdActividades();
        return id;
    } catch (error) {
        throw error;
    }
}

router.post('/nuevaActividad', async (req, res) => {
    let actividad = req.body;
    try {
        let idActividad = await idAleatorioActividad();
        query = "INSERT INTO actividadesroca(idActividad, nombreActividad, statusActividad, idRoca, idUsuario) VALUES(?,?,?,?,?)";
        valores = [idActividad, actividad.nombreActividad, actividad.statusActividad, actividad.idRoca, actividad.idUsuario];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Registrado correctamente' });
            } else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
})

router.patch('/editarActividad', async (req, res) => {
    let actividad = req.body;
    try {
        query = "UPDATE actividadesRoca SET nombreActividad=? WHERE idActividad=?";
        valores = [actividad.nombreActividad, actividad.idActividad];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Actuaizado correctamente' });
            } else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
})

router.patch('/archivarActividad', async (req, res) => {
    let actividad = req.body;
    try {
        query = "UPDATE actividadesroca SET statusActividad='Archivado' WHERE idActividad=?";
        valores = [actividad.idActividad];
        conexion.query(query, valores, (err, results) => {
            if (!err) {
                return res.status(200).json({ mensaje: 'Actuaizado correctamente' });
            } else {
                return res.status(500).json({ mensaje: err.sqlMessage });
            }
        });
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
})

router.get('/obtenerActividades/:idRoca', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idRoca = req.params.idRoca;
    var query = "SELECT idActividad, nombreActividad, statusActividad FROM actividadesroca  WHERE idRoca=?";
    conexion.query(query, [idRoca], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerActividadesSecundarias/:idUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idUsuario = req.params.idUsuario;
    var query = "SELECT idActividad, nombreActividad, statusActividad FROM actividadesroca WHERE idRoca IS NULL AND idUsuario = ?";
    conexion.query(query, [idUsuario], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    });
});


router.get('/verificarDia/:fecha/:idActividad', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idActividad = req.params.idActividad;
    const fecha = req.params.fecha;
    var query = "SELECT statusActividad FROM seguimientoActividades  WHERE idActividad=? AND fechaActividad=?";
    conexion.query(query, [idActividad, fecha], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    })
})




router.post('/planearActividad', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let actividad = req.body;

    var query = "INSERT INTO seguimientoActividades(fechaActividad, statusActividad, idActividad) VALUES (?,?,?)";
    conexion.query(query, [actividad.fechaActividad, actividad.statusActividad, actividad.idActividad], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.patch('/actualizarStatusActividad', auth.autentificarToken, checkRole.checkRole, (req, res, next) => {
    let actividad = req.body;
    var query = "UPDATE seguimientoActividades set statusActividad=? WHERE idActividad=? AND fechaActividad=?";
    conexion.query(query, [actividad.statusActividad, actividad.idActividad, actividad.fechaActividad], (err, results) => {
        if (!err) {
            if (results.affectedRows == 0) {
                return res.status(404).json(err);
            }
            return res.status(200).json({ mensaje: "Actualizado correctamente" });
        }
        else {
            return res.status(500).json(err);
        }
    })
})

router.get('/obtenerColaboradores/:idUsuario', auth.autentificarToken, checkRole.checkRole, (req, res) => {
    const idUsuario = req.params.idUsuario;
    var query = `
        WITH RECURSIVE JerarquiaUsuarios AS (
            SELECT idUsuario, nombreUsuario, apellidoUsuario, jefeDirectoUsuario, puestoUsuario, statusUsuario FROM usuarios WHERE idUsuario = ?
            UNION ALL
            SELECT u.idUsuario, u.nombreUsuario, u.apellidoUsuario, u.jefeDirectoUsuario, u.puestoUsuario, u.statusUsuario
            FROM usuarios u
            JOIN JerarquiaUsuarios es ON u.jefeDirectoUsuario = es.idUsuario
        )
        SELECT * FROM JerarquiaUsuarios;
        `;
    conexion.query(query, [idUsuario], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        } else {
            return res.status(500).json(err);
        }
    });
});






module.exports = router;